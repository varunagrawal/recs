RECS
====

 - By Varun Agrawal (varunagrawal)

		Rank Expert Classification System (RECS)

			Machine Learning for Student Counselling

The RECS system hopes to provide a reliable and efficient way for students to get suggestions regarding college selection during the counselling round of competitive exams such as the All India Engineering Entrance Exam (AIEEE) and the Maharashtra Common Entrance Test (MH-CET).

Many students are either confused or misdirected on the right choice of colleges for them to select for admission purposes. The RECS system aims to help these students by using Machine Learning and Data Mining to find the 10 best colleges that suit their exam ranks and stream of study preferences.

This system is general enough to be adapted enough to any examination and governing body. It can be easily further customized to provide more features to students.

Usage:
	After an initial training phase involving admission data from  the previous n (>= 5) years, all a user of the system has to provide is his rank in the examination, and optionally, his preferences for institute and field of study. The system than mines the training data to find the 10 best matches for the user's credentials.

Hope you find this software useful.
